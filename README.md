# CSV_IO


A single file libary for CSV Input/Output in Fortran.

This module is largely adapted from [John Burkardt's](https://people.sc.fsu.edu/~jburkardt/f_src/csv_io/csv_io.html).


## Requirements
* [PorPre](https://gitlab.com/RomainNoel/porpre) a portable precision library
